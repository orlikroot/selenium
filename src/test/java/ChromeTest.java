import junit.framework.TestCase;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RunWith(BlockJUnit4ClassRunner.class)
public class ChromeTest extends TestCase {

    private static ChromeDriverService service;
    private WebDriver driver;

    @BeforeClass
    public static void createAndStartService() throws IOException {
        service = new ChromeDriverService.Builder()
                .usingChromeDriverExecutable(new File("c://Test/chromedriver.exe"))
                .usingAnyFreePort()
                .build();
        service.start();
    }

    @AfterClass
    public static void createAndStopService() {
//        service.stop();
    }

    @Before
    public void createDriver() {
        driver = new RemoteWebDriver(service.getUrl(),
                DesiredCapabilities.chrome());
    }

    @After
    public void quitDriver() {
//        driver.quit();
    }

//    @Test
//    public void testGoogleSearch() {
//        driver.get("http://www.google.com");
//        // rest of the test...
//    }

    @Test
    public void webDriverTest2(){
        driver.get("http://mail.ru");
        WebElement select = driver.findElement(By.xpath("//select"));
        List<WebElement> allOptions = select.findElements(By.tagName("option"));
        for (WebElement option : allOptions) {
            System.out.println(String.format("��������: %s", option.getAttribute("value")));
            if (option.getAttribute("value").equals("mail.ua")){
                option.click();
            }
        }

    }

    @Test
    public void webDriverTest3register(){
        driver.get("http://mail.ru");
        WebElement searchRegister = driver.findElement(By.className("mailbox__register__link"));
        driver.findElement(By.className("mailbox__register__link")).click();
        //driver.findElement(By.xpath("//input[@type='text']")));//.sendKeys("1234123412341234");
        WebElement select11 = driver.findElement(By.xpath("//select[@class='fll days mt0 mb0 qc-select-day']"));
        List<WebElement> allOptions = select11.findElements(By.xpath("child::option"));
        for (WebElement option : allOptions) {
            System.out.println(String.format("��������: %s", option.getAttribute("value")));
            if (option.getAttribute("value").equals("6")){
                option.click();
            }
//		    WebElement select2 = driver.findElement(By.xpath("//select[@class='fll months mt0 mb0 qc-select-month']"));
//			List<WebElement> allOptions2 = select2.findElements(By.xpath("//*/div[4]/span[2]/select[2]/option/*"));
            //		for (WebElement option2 : allOptions2) {
            //	    System.out.println(String.format("��������: %s", option.getAttribute("value")));
            //    if (option2.getAttribute("value").equals("����")){
            //  option2.click();
            //}
            //}
        }
        //driver.findElement(By.className("fll days mt0 mb0 qc-select-day"));
    }
}
